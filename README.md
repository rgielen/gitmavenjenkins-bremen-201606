# Test Project

This is a test project for Git-Maven-Jenkins training course.
It is totally important to follow the history.
It will evolve during the four course days.
The canonical repository is found at https://bitbucket.org/rgielen/gitmavenjenkins-bremen-201606 .
This will be a Java project later on.Traiuning attendants will have only read access.

See LICENSE.txt for further information.
