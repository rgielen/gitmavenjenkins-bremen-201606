package net.rgielen.gmjkurs;

public class HelloWorld {

	public static void main (String [] args) {
		new HelloWorld().sayHello();
	}

	private void sayHello() {
		System.out.println(getHelloMessage());
	}

	String getHelloMessage() {
		return "Hello";
	}
}
