package net.rgielen.gmjkurs;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public class HelloWorldTest {

    @Test
    public void testExpectedMessageIsHello() throws Exception {
        assertEquals("Hello", new HelloWorld().getHelloMessage());
    }

    @Ignore @Test
    public void testExpectedMessageIsHallo() throws Exception {
        assertEquals("Hallo", new HelloWorld().getHelloMessage());
    }

}